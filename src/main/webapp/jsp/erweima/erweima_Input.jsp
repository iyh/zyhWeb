<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>输入二维码内容</title>
<!-- 静态包含jsp 通用的head所需的标签 （静态包含是先合并jsp再转换为servlet） -->
<%@include file="/jsp/common/head.jsp"%>
<!-- 引入jstl  -->
<%@include file="/jsp/common/tag.jsp"%>
</head>
<body>
	<form role="form" action="<%=request.getContextPath()%>/erweima/string" method="post">
		<div class="form-group">
			<label for="content">文字内容</label> <input type="text" class="form-control"
				id="content" placeholder="请输入生成二维码的文字"  name="content">
		</div>
		<button type="submit" class="btn btn-default">提交</button>
	</form>
</body>
</html>