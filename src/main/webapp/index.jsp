<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%  session.setAttribute("user", "中文名"); %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<!-- 静态包含jsp 通用的head所需的标签 （静态包含是先合并jsp再转换为servlet） -->
<%@include file="/jsp/common/head.jsp"%>
<!-- 引入jstl  -->
<%@include file="/jsp/common/tag.jsp"%>
</head>
<body>
	<table align="centre" border="1">
		<tr>
			<td>Session ID</td>
			<td><%=session.getId()%></td>
		</tr>
		<tr>
			<td>Created on</td>
			<td><%=session.getCreationTime()%></td>
		</tr>
		<tr>
			<td>ServerName</td>
			<td><%=request.getServerName()%></td>
		</tr>
		<tr>
			<td>ServerPort:</td>
			<td><%=request.getServerPort()%></td>
		</tr>
		<tr>
			<td>getAttribute:user</td>
			<td><%=request.getSession().getAttribute("user")%></td>
		</tr>
		<tr>
			<td>Say:</td>
			<td>This is Tomcat Server 8080</td>
		</tr>
	</table>
	<br>
	<hr>
	<a href="jsp/erweima/erweima_Input.jsp">二维码</a>
	<a href="jsp/BootstrapTest/boot1.html">BootStrap练习</a><br>
</body>
</html>