package yh.JFreeChart;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.ui.TextAnchor;

/**
 * 
 * @Title:JFreeChart实现柱状图 
 * @Description:生成图片到图形框架面板
 * @author 张颖辉
 * @date 2017年1月24日上午10:43:44
 * @version 1.0
 */
public class JFreeChartPillar {
	/**
	 * 获得数据集。
	 * 
	 * @return org.jfree.data.category.CategoryDataset
	 */
	private static CategoryDataset getDataset1() {
		double[][] data = new double[][] { { 751, 800, 260, 600, 200 }, { 400, 560, 240, 300, 150 },
				{ 600, 450, 620, 220, 610 } };
		String[] rowKeys = { "CPU", "硬盘", "内存" };
		String[] columnKeys = { "北京", "上海", "广州", "南京", "深圳" };
		CategoryDataset dataset = DatasetUtilities.createCategoryDataset(rowKeys, columnKeys, data);
		return dataset;
	}

	/**
	 * 生成柱状图。
	 */
	public static void main(String[] args) {
		String title = "统计表";
		// 获得数据集
		CategoryDataset dataset = getDataset1();
		JFreeChart chart = ChartFactory.createBarChart3D(title, // 图表标题
				"时间", // 目录轴的显示标签
				"次数", // 数值轴的显示标签
				dataset, // 数据集
				PlotOrientation.VERTICAL, // 图表方向：水平、垂直
				true, // 是否显示图例
				true, // 是否生成工具（提示）
				false // 是否生成URL链接
		);
		// 设置标题字体
		Font font = new Font("宋体", Font.BOLD, 18);
		TextTitle textTitle = new TextTitle(title);
		textTitle.setFont(font);
		chart.setTitle(textTitle);
		chart.setTextAntiAlias(false);
		// 设置背景色
		chart.setBackgroundPaint(new Color(255, 255, 255));
		// 设置图例字体
		LegendTitle legend = chart.getLegend(0);
		legend.setItemFont(new Font("宋体", Font.TRUETYPE_FONT, 14));
		// 获得柱状图的Plot对象
		CategoryPlot plot = chart.getCategoryPlot();
		BarRenderer3D customBarRenderer = (BarRenderer3D) plot.getRenderer();
		// 取得横轴
		CategoryAxis categoryAxis = plot.getDomainAxis();
		// 设置横轴显示标签的字体
		categoryAxis.setLabelFont(new Font("宋体", Font.BOLD, 16));
		// 设置横轴标记的字体
		categoryAxis.setTickLabelFont(new Font("宋休", Font.TRUETYPE_FONT, 16));
		// 取得纵轴
		NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
		// 设置纵轴显示标签的字体
		numberAxis.setLabelFont(new Font("宋体", Font.BOLD, 16));
		numberAxis.setTickLabelFont(new Font("Fixedsys", Font.PLAIN, 13));
		customBarRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());// 显示每个柱的数值
		customBarRenderer.setBaseItemLabelsVisible(true);
		// 注意：此句很关键，若无此句，那数字的显示会被覆盖，给人数字没有显示出来的问题
		customBarRenderer.setBasePositiveItemLabelPosition(
				new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));
		ChartFrame frame = new ChartFrame(title, chart, true);
		frame.pack();
		frame.setVisible(true);
	}
}
