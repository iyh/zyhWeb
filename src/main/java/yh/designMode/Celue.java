package yh.designMode;

/**
 * @Title:Celue
 * @Description:设计模式1：策略模式
 * @author 张颖辉
 * @date 2017年3月20日下午6:36:47
 * @version 1.0
 */
public class Celue {
	/**
	 * @Description:鸭子超类
	 * @author 张颖辉
	 * @date 2017年3月20日下午6:59:12
	 * @version 1.0
	 */
	public abstract class Duck {
		private QuackBehavior quackBehavior;
		private FlyBehavior flyBehavior;

		public Duck() {    

		}

		// 实现方式多的行为-叫
		public void quack() {          
			quackBehavior.quack();
		}

		// 实现方式多的行为-飞
		public void fly() {
			flyBehavior.fly();
		}

		// 那些实现固定的行为-游泳
		public void swim() {
			System.out.println("所有的鸭子都会游泳");
		}

		// 抽象方法：炫耀
		public abstract void display();

		/** getter+setter ***/
		public void setQuackBehavior(QuackBehavior quackBehavior) {
			this.quackBehavior = quackBehavior;
		}

		public void setFlyBehavior(FlyBehavior flyBehavior) {
			this.flyBehavior = flyBehavior;
		}

	}

	// 叫的动作
	public interface QuackBehavior {
		void quack();
	}

	// 飞的动作
	public interface FlyBehavior {
		void fly();
	}

	/** 各种叫的动作的实现 **/
	public class Quack implements QuackBehavior {

		@Override
		public void quack() {
			System.out.println("嘎嘎");
		}

	}

	public class Squeack implements QuackBehavior {

		@Override
		public void quack() {
			System.out.println("吱吱");
		}

	}

	public class MuteQuack implements QuackBehavior {

		@Override
		public void quack() {
			// 哑巴鸭子，不会叫
		}
	}

	/** 各种飞的动作的实现 **/
	public class FlyWithWings implements FlyBehavior {

		@Override
		public void fly() {
			System.out.println("飞啊飞啊");
		}

	}

	public class FlyNoWay implements FlyBehavior {

		@Override
		public void fly() {
			// 不会飞行
		}
	}

	/**
	 * @Description:鸭子的继承类：野鸭
	 * @author 张颖辉
	 * @date 2017年3月20日下午7:00:50
	 * @version 1.0
	 */
	public class MallardDuck extends Duck {
		public MallardDuck() {//初始化时指定行为的实现方式
			setQuackBehavior(new Quack());
			setFlyBehavior(new FlyWithWings());
		}

		@Override
		public void display() {
			System.out.println("我是一只小野鸭！~");
		}
	}

	// 测试
	public static void main(String[] args) {
		Duck mallardDuck = new Celue().new MallardDuck();
		mallardDuck.fly();
		mallardDuck.quack();
		mallardDuck.setQuackBehavior(new Celue().new Squeack());
		mallardDuck.quack();
		/**
		 * 将Duck中实现多的行为单独提出来作为接口，再实现各种具体行为，超类Duck中对应的方法并不是真正的实现，而是调用持有的特定行为的引用。
		 * 在超类的具体子类MallardDuck中初始化时赋予功能接口变量真正的实现。之后MallardDuck实例调用的动作就是该实例的真正实现。
		 * 将来增加新的实现，只需要增加接口的实现类即可，便于维护 总结：这样设计可以避免需求频繁变动需要修改或增加功能时对已有实现的影响。
		 */
	}

}
