package yh.designMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Title:观察者模式样例
 * @Description:《Head First设计模式》学习笔记
 * @author 张颖辉
 * @date 2017年5月3日上午10:07:09
 * @version 1.0
 */
public class GuanChaZhe {
	/**
	 * 接口：主题，被观察（订阅）的对象
	 * 
	 * @author rigour
	 *
	 */
	public interface Subject {
		/**
		 * 注册成为观察者
		 * 
		 * @param o
		 */
		public void registerObserver(Observer o);

		/**
		 * 删除观察者
		 * 
		 * @param o
		 */
		public void removeObserver(Observer o);

		/**
		 * 通知所有观察者 当主题改变时调用的方法
		 */
		public void notifyObserver();
	}

	/**
	 * 接口：观察者，被通知的对象
	 * 
	 * @author rigour
	 *
	 */
	public interface Observer {
		/**
		 * 通知时调用此方法，传递气象数值
		 * 
		 * @param temp
		 *            温度
		 * @param humidity
		 *            湿度
		 * @param pressure
		 *            气压
		 */
		public void update(float temp, float humidity, float pressure);
	}

	/**
	 * 接口：布告板
	 * 
	 * @author rigour
	 *
	 */
	public interface DisplayElement {
		/**
		 * 显示数据
		 */
		public void display();
	}

	/** ###实现类#### **/
	/**
	 * @Title:气象数据对象
	 * @Description:检测的气象值保存在这里
	 * @author 张颖辉
	 * @date 2017年5月3日上午10:27:40
	 * @version 1.0
	 */
	public class WeatherData implements Subject {
		private List<Observer> observers;
		private float temp;// 温度
		private float humidity;// 湿度
		private float pressure;// 气压

		public WeatherData() {
			observers = new ArrayList<Observer>();
		}

		@Override
		public void registerObserver(Observer o) {
			observers.add(o);

		}

		@Override
		public void removeObserver(Observer o) {
			observers.remove(o);

		}

		@Override
		public void notifyObserver() {
			for (Observer observer : observers) {
				observer.update(temp, humidity, pressure);
			}
		}

		/**
		 * @Title:主题的监测值改变则通知所有观察者
		 * @Description:Comment for non-overriding methods
		 * @author 张颖辉
		 * @date 2017年5月3日上午10:51:53
		 */
		public void measurementsChanged() {
			notifyObserver();
		}

		/**
		 * @Title:设置主题的监测值
		 * @Description:Comment for non-overriding methods
		 * @author 张颖辉
		 * @date 2017年5月3日上午10:51:35
		 * @param temp
		 * @param humidity
		 * @param pressure
		 */
		public void setMeasurements(float temp, float humidity, float pressure) {
			this.temp = temp;
			this.humidity = humidity;
			this.pressure = pressure;
			measurementsChanged();
		}
		// ...
	}

	/**
	 * @Title:布告板-当前气象值
	 * @Description:Comment for created type
	 * @author 张颖辉
	 * @date 2017年5月3日上午10:10:40
	 * @version 1.0
	 */
	public class CurrentConditionsDisplay implements Observer, DisplayElement {
		private float temp;// 温度
		private float humidity;// 湿度
		private float pressure;// 气压

		/**
		 * @Title:构造器-同时注册成为观察者
		 * @Description:Comment for created constructors
		 * @author 张颖辉
		 * @date 2017年5月3日上午11:14:51
		 * @param weatherData
		 */
		public CurrentConditionsDisplay(WeatherData weatherData) {
			weatherData.registerObserver(this);
		}

		@Override
		public void display() {
			System.out.println("布告板-当前气象值：" + toString());
		}

		@Override
		public void update(float temp, float humidity, float pressure) {
			this.temp = temp;
			this.humidity = humidity;
			this.pressure = pressure;
			display();
		}

		@Override
		public String toString() {
			return "CurrentConditionsDisplay [temp=" + temp + ", humidity=" + humidity + "]";
		}

	}

	/**
	 * @Title:布告板-气象统计
	 * @Description:Comment for created type
	 * @author 张颖辉
	 * @date 2017年5月3日上午10:12:11
	 * @version 1.0
	 */
	public class StatisticsDisplay implements Observer, DisplayElement {
		private float temp;// 温度
		private float humidity;// 湿度
		private float pressure;// 气压

		public StatisticsDisplay(WeatherData weatherData) {
			weatherData.registerObserver(this);
		}

		@Override
		public void display() {
			System.out.println("布告板-气象统计：" + toString());
		}

		@Override
		public void update(float temp, float humidity, float pressure) {
			this.temp = temp;
			this.humidity = humidity;
			this.pressure = pressure;
			display();
		}

		@Override
		public String toString() {
			return "StatisticsDisplay [temp=" + temp + ", humidity=" + humidity + ", pressure=" + pressure + "]";
		}

	}

	/**
	 * 
	 * @Title:布告板-天气预报
	 * @Description:Comment for created type
	 * @author 张颖辉
	 * @date 2017年5月3日上午10:14:32
	 * @version 1.0
	 */
	public class ForecastDisplay implements Observer, DisplayElement {
		private float temp;// 温度
		private float humidity;// 湿度
		private float pressure;// 气压

		public ForecastDisplay(WeatherData weatherData) {
			weatherData.registerObserver(this);
		}

		@Override
		public void display() {
			System.out.println("布告板-天气预报：" + toString());
		}

		@Override
		public void update(float temp, float humidity, float pressure) {
			this.temp = temp;
			this.humidity = humidity;
			this.pressure = pressure;
			display();
		}

		@Override
		public String toString() {
			return "ForecastDisplay [temp=" + temp + ", humidity=" + humidity + ", pressure=" + pressure + "]";
		}

	}

	public static void main(String[] args) throws InterruptedException {
		//创建主题-气象数据对象（被订阅者）
		WeatherData weatherData = new GuanChaZhe().new WeatherData();
		//创建观察者（订阅者）对象，同时已经注册成为气象数据对象的观察者
		new GuanChaZhe().new CurrentConditionsDisplay(weatherData);
		new GuanChaZhe().new StatisticsDisplay(weatherData);
		new GuanChaZhe().new ForecastDisplay(weatherData);
		int i = 0;
		while (true) {
			i++;
			System.out.println("--------------气象数据（主题）更新--------------");
			weatherData.setMeasurements(100 + i, 20 + i, 1000 + i);
			Thread.sleep(1000);
		}
		
//		--------------气象数据（主题）更新--------------
//		布告板-当前气象值：CurrentConditionsDisplay [temp=101.0, humidity=21.0]
//		布告板-气象统计：StatisticsDisplay [temp=101.0, humidity=21.0, pressure=1001.0]
//		布告板-天气预报：ForecastDisplay [temp=101.0, humidity=21.0, pressure=1001.0]
//		--------------气象数据（主题）更新--------------
//		布告板-当前气象值：CurrentConditionsDisplay [temp=102.0, humidity=22.0]
//		布告板-气象统计：StatisticsDisplay [temp=102.0, humidity=22.0, pressure=1002.0]
//		布告板-天气预报：ForecastDisplay [temp=102.0, humidity=22.0, pressure=1002.0]
//		--------------气象数据（主题）更新--------------
//		布告板-当前气象值：CurrentConditionsDisplay [temp=103.0, humidity=23.0]
//		布告板-气象统计：StatisticsDisplay [temp=103.0, humidity=23.0, pressure=1003.0]
//		布告板-天气预报：ForecastDisplay [temp=103.0, humidity=23.0, pressure=1003.0]
//		--------------气象数据（主题）更新--------------
//		布告板-当前气象值：CurrentConditionsDisplay [temp=104.0, humidity=24.0]
//		布告板-气象统计：StatisticsDisplay [temp=104.0, humidity=24.0, pressure=1004.0]
//		布告板-天气预报：ForecastDisplay [temp=104.0, humidity=24.0, pressure=1004.0]
	}
}
