package yh.web.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import yh.web.service.ErweimService;

@Controller
@RequestMapping("/erweima")
public class ErweimController {

	@Autowired
	private ErweimService erweimService;

	/**
	 * @Title:生成二维码（无图）
	 * @Description:restful格式的get请求
	 * @author 张颖辉
	 * @date 2017年1月3日下午2:38:05
	 * @param content
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/stringUrl/{content}", method = RequestMethod.GET)
	public String erweimByStringUrl(@PathVariable("content") String content, HttpServletRequest request, Model model) {
		ServletContext servletContext = request.getServletContext();
		String filePath = erweimService.stringOnly(content, servletContext);
		model.addAttribute("filePath", filePath);
		return "erweima/erweima";
	}

	/**
	 * @Title:生成二维码（无图）
	 * @Description:post表单请求
	 * @author 张颖辉
	 * @date 2016年12月6日下午5:55:33
	 * @param content
	 * @param request
	 * @param model
	 * @return
	 */
	
	@RequestMapping(value = "/string", method = RequestMethod.POST)
	public String erweimByString(@RequestParam("content") String content, HttpServletRequest request, Model model) {
		ServletContext servletContext = request.getServletContext();
		String filePath = erweimService.stringOnly(content, servletContext);
		model.addAttribute("filePath", filePath);
		return "erweima/erweima";
	}
}
