package yh.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import yh.web.pojo.TbUser;
import yh.web.service.TbUserService;

@Controller
@RequestMapping("/tbUser")
public class TbUserController {
	@Autowired
	private TbUserService tbUserService;

	@ResponseBody
	@RequestMapping(value = "/save")
	public Map<String, Object> save(Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		TbUser tbUser = new TbUser();
		tbUser.setId(id);
		tbUser.setCreated(new Date());
		tbUser.setUpdated(new Date());
		tbUserService.save(tbUser);
		result.put("tbUser", tbUser);
		return result;
	}

	// 传统格式不能使用@PathVariable("content") 关键字不然会报406错误
	@ResponseBody
	@RequestMapping(value = "/get")
	public Map<String, Object> get(Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		TbUser tbUser = tbUserService.get(id);
		result.put("tbUser", tbUser);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/update")
	public int update(Long id) {
		TbUser tbUser = tbUserService.get(id);
		tbUser.setUpdated(new Date());
		return tbUserService.update(tbUser);
	}

	@ResponseBody
	@RequestMapping(value = "/delete")
	public int delete(Long id) {
		return tbUserService.delete(id);
	}
}
