package yh.web.service;

import yh.web.pojo.TbUser;

public interface TbUserService {
	public void save(TbUser tbUser);

	public TbUser get(Long id);

	public int update(TbUser tbUser);
	
	public int delete(Long id);
}
