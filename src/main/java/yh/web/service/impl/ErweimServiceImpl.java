package yh.web.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import yh.web.service.ErweimService;
import yh.web.util.EWCodeUtil;

@Service
public class ErweimServiceImpl implements ErweimService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	// @Autowired
	// private WebApplicationContext wac;

	@Override
	public String stringOnly(String content, ServletContext servletContext) {
		// 创建目录
		// String path = "D:";
		String savePath = "/upload/2wm/string";
		String realPath = servletContext.getRealPath(savePath);
		//D:\Workspaces\yh\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\zyhWeb\\upload\2wm\string
		System.out.println("realPath:"+realPath);
		File realDir = new File(realPath);
		if (!realDir.exists()) {
			realDir.mkdirs();// 目录不存在的情况下，创建目录。
		}
		String fileName = UUID.randomUUID().toString() + ".jpg";
		try {

			MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
			// 内容所使用编码(有中文则必须指定编码)
			Map<EncodeHintType, String> hints = new HashMap<EncodeHintType, String>();
			hints.put(EncodeHintType.CHARACTER_SET, "UTF8");
			BitMatrix bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, 200, 200, hints);
			// 生成二维码
			File outputFile = new File(realPath, fileName);
			EWCodeUtil.writeToFile(bitMatrix, "jpg", outputFile);
		} catch (WriterException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return savePath+"/"+fileName;
	}

}
