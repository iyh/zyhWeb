package yh.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import yh.web.mapper.TbUserMapper;
import yh.web.pojo.TbUser;
import yh.web.service.TbUserService;

@Service
public class TbUserServiceImpl implements TbUserService {
	@Autowired
	private TbUserMapper tbUserMapper;
	
	@Override
	@Transactional
	public void save(TbUser tbUser){
		tbUserMapper.insert(tbUser);
	}

	@Override
	public TbUser get(Long id) {
		return tbUserMapper.selectByPrimaryKey(id);
	}

	@Override
	@Transactional
	public int update(TbUser tbUser) {
		return tbUserMapper.updateByPrimaryKey(tbUser);
	}

	@Override
	@Transactional
	public int delete(Long id) {
		return tbUserMapper.deleteByPrimaryKey(id);
	}

}
