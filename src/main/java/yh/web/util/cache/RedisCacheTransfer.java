package yh.web.util.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

/**
 * @Title:静态注入中间类
 * @Description:Comment for created type
 * @author 张颖辉
 * @date 2017年2月15日上午11:18:42
 * @version 1.0
 */
public class RedisCacheTransfer 
{

    @Autowired
    public void setJedisConnectionFactory(JedisConnectionFactory jedisConnectionFactory) {
        RedisCache.setJedisConnectionFactory(jedisConnectionFactory);
    }

}