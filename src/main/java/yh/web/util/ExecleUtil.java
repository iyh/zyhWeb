package yh.web.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @Title:ExecleUtil
 * @Description:专为excel建立的工具类，支持97-2003 以及2007版本
 * @author 张颖辉
 * @date 2017年5月15日下午8:23:12
 * @version 1.0
 */
public class ExecleUtil {
	/**
	 * Excel column index begin 0
	 * 
	 * @Description:Excel 列号转换， A 转 0，，从0开始
	 * @param colStr
	 * @param length
	 * @return
	 */
	public static int excelColStrToNum(String colStr) {
		int length = colStr.length();
		int num = 0;
		int result = 0;
		for (int i = 0; i < length; i++) {
			char ch = colStr.charAt(length - i - 1);
			num = (int) (ch - 'A' + 1);
			num *= Math.pow(26, i);
			result += num;
		}
		result--;
		return result;
	}

	/**
	 * Excel column index begin 0
	 * 
	 * @Description:Excel 列号转换， A 转 0，从0开始
	 * @param columnIndex
	 * @return
	 */
	public static String excelColIndexToStr(int columnIndex) {
		columnIndex++;
		if (columnIndex <= 0) {
			return null;
		}
		String columnStr = "";
		columnIndex--;
		do {
			if (columnStr.length() > 0) {
				columnIndex--;
			}
			columnStr = ((char) (columnIndex % 26 + (int) 'A')) + columnStr;
			columnIndex = (int) ((columnIndex - columnIndex % 26) / 26);
		} while (columnIndex > 0);
		return columnStr;
	}

	/**
	 * 字符串的第一个数字
	 * 
	 * @param str
	 * @return
	 */
	private static int firstDigit(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= '0' && c <= '9') {
				return c - '0';
			}
		}
		return -1;
	}

	/**
	 * @Title:通过excel坐标地址读取cell的String内容
	 * @Description:Comment for non-overriding methods
	 * @author 张颖辉
	 * @date 2017年5月15日下午8:23:01
	 * @param sheet
	 * @param address
	 * @return
	 */
	public static String readCell2String(Sheet sheet, String address) {
		String value = null;
		Cell cell = getCell(sheet, address);
		value = cell.getStringCellValue();
		return value;

	}

	/**
	 * @Title:通过excel坐标地址获取cell
	 * @Description:Comment for non-overriding methods
	 * @author 张颖辉
	 * @date 2017年5月15日下午8:26:08
	 * @param sheet
	 * @param address
	 * @return
	 */
	public static Cell getCell(Sheet sheet, String address) {
		int firstDigit = firstDigit(address);
		int index = address.indexOf(firstDigit + "");
		int rowNum = Integer.valueOf(address.substring(index)) - 1;
		int columNum = excelColStrToNum(address.substring(0, index));
		Row row = sheet.getRow(rowNum);
		return row.getCell(columNum);

	}

	/**
	 * @Title:获取Workbook
	 * @Description:根据文件后缀名，创建不同版本的Workbook
	 * @author 张颖辉
	 * @date 2017年5月26日上午10:46:25
	 * @param filePath
	 * @return
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public static Workbook getWorkbook(String filePath)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		Workbook workbook = null;
		String houz = filePath.substring(filePath.lastIndexOf("."));
		InputStream inputStream = new FileInputStream(filePath);
		//workbook = WorkbookFactory.create(inputStream); //网上说这种办法最好，可以兼容97-2003 和2007版本的excel
		if (".xls".equals(houz)) {
			// 97-2003
			workbook = new HSSFWorkbook(inputStream);
		} else {
			// 2007
			workbook = new XSSFWorkbook(inputStream);
		}
		return workbook;

	};

	/** ########### 下面是测试 ############### **/
	public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException {
		String path = "E:/workspaces/eclipse_spaces/pool_ssgl/pool/WebRoot/temple/aginst/upload/DL_64 - 副本.xlsx";
		read(path);
		write(path);
		read(path);
	}

	private static void read(String filePath) throws IOException, EncryptedDocumentException, InvalidFormatException {
		Workbook wb = ExecleUtil.getWorkbook(filePath);
		Sheet sheet = wb.getSheetAt(0);
		System.out.println(ExecleUtil.readCell2String(sheet, "AG59"));
		wb.close();
	}

	private static void write(String filePath) throws IOException, EncryptedDocumentException, InvalidFormatException {
		Workbook wb = ExecleUtil.getWorkbook(filePath);
		Sheet sheet = wb.getSheetAt(0);
		Cell cell = ExecleUtil.getCell(sheet, "AG59");
		// System.out.println(cell.getStringCellValue());
		cell.setCellValue("孙贺男");
		// System.out.println(cell.getStringCellValue());
		OutputStream stream = new FileOutputStream(filePath);
		wb.write(stream);
		stream.close();
		wb.close();
	}
}
