package yh.core.enumDemo;
/**
 * 
 * @Title:枚举的标准模型
 * @Description:一般使用在返回信息的封装
 * @author 张颖辉
 * @date 2017年1月16日上午11:34:04
 * @version 1.0
 */
public enum SeckillStateEnum {
	SUCCESS(1, "秒杀成功"), END(0, "秒杀结束"), REPEAT_KILL(-1, "重复秒杀"), INNER_ERROR(-2, "系统异常"), DATE_REWRITE(-3, "数据篡改");

	private int state;// 结果状态码

	private String stateInfo;// 结果状态信息


	private SeckillStateEnum(int state, String stateInfo) {
		this.state = state;
		this.stateInfo = stateInfo;
	}

	public static SeckillStateEnum stateOf(int state) {
		for (SeckillStateEnum stateEnum : values()) {
			if (stateEnum.getState() == state) {
				return stateEnum;
			}
		}
		return null;

	}

	public int getState() {
		return state;
	}

	public String getStateInfo() {
		return stateInfo;
	}
	public static void main(String[] args) {
		
		String s=SeckillStateEnum.END.getStateInfo();
		System.out.println(s);
	}

}
