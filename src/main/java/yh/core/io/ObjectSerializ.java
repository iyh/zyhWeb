package yh.core.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

/**
 * @Title:对象序列化demo
 * @Description:《分布式学习练习》1.1.2
 * @author 张颖辉
 * @date 2016年12月27日下午1:11:49
 * @version 1.0
 */
public class ObjectSerializ {
	private static Logger logger = LoggerFactory.getLogger(ObjectSerializ.class);

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Obj obj = new Obj("哇哈哈", 100);
		serializByJava(obj);
		serializByHessian(obj);
	}

	/**
	 * @Title:Java对象序列化
	 * @Description:java内置的对象序列化
	 * @author 张颖辉
	 * @date 2016年12月27日下午1:41:09
	 * @param obj
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void serializByJava(Obj obj) throws IOException, ClassNotFoundException {
		// 序列化
		ByteArrayOutputStream byteOStream = new ByteArrayOutputStream();
		ObjectOutputStream objOStream = new ObjectOutputStream(byteOStream);
		objOStream.writeObject(obj);

		byte[] byteArray = byteOStream.toByteArray();
		// 反序列化
		ByteArrayInputStream byteIStream = new ByteArrayInputStream(byteArray);
		ObjectInputStream objIStream = new ObjectInputStream(byteIStream);
		Obj obj2 = (Obj) objIStream.readObject();
		logger.info("obj2={}", obj2.toString());
	}

	/**
	 * 
	 * @Title:Hessian对象序列化(推荐)
	 * @Description:比java内置的方式效率高很多
	 * @author 张颖辉
	 * @date 2016年12月27日下午1:41:38
	 * @param obj
	 * @throws IOException
	 */
	public static void serializByHessian(Obj obj) throws IOException {
		// 序列化
		ByteArrayOutputStream byteOStream = new ByteArrayOutputStream();
		HessianOutput hesOput = new HessianOutput(byteOStream);
		hesOput.writeObject(obj);

		byte[] byteArray = byteOStream.toByteArray();
		// 反序列化
		ByteArrayInputStream byteIStream = new ByteArrayInputStream(byteArray);
		HessianInput hesIput = new HessianInput(byteIStream);
		Obj obj2 = (Obj) hesIput.readObject();
		logger.info("obj2={}", obj2.toString());
	}
}

class Obj implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Integer num;

	public Obj() {
		super();

	}

	public Obj(String name, Integer num) {
		super();
		this.name = name;
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "Obj[name=" + name + ", num=" + num + "]";
	}

}