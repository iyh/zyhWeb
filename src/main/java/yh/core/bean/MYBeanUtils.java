package yh.core.bean;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

//import org.apache.commons.beanutils.BeanUtils;//没起作用 不知道为啥
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapperImpl;

public class MYBeanUtils {
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {

		MyBean src = new MyBean();
		src.setStatus((byte) 1);
		// o = new MyBean(2, "无名小卒2", "终南山上2", '男', (byte) 2);
		MyBean target = new MyBean(1, "无名小卒", "终南山上", '男', (byte) 0);
		System.out.println("本来的样子：" + target);
		System.out.println("***springframework**copyProperties****");

		BeanUtils.copyProperties(src, target);
		// (Object 目标, Object 源点)

		System.out.println("源点o=" + src);
		System.out.println("目标d=" + target);
		System.out.println("***springframework*copyPropertiesIgnoreNull*****");
		src = new MyBean();
		src.setStatus((byte) 1);
		target = new MyBean(1, "无名小卒", "终南山上", '男', (byte) 0);
		copyPropertiesIgnoreNull(src, target);
		System.out.println("源点o=" + src);
		System.out.println("目标d=" + target);

	}

	/**
	 * @Title:属性克隆（类型和名称相同的属性）
	 * @Description:可以是两个不想管的对象，只要属性的类型和名称一样就会被克隆（空值属性会被忽略）
	 * @author 张颖辉
	 * @date 2017年2月4日下午2:32:53
	 * @param src
	 * @param target
	 */
	public static void copyPropertiesIgnoreNull(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	private static String[] getNullPropertyNames(Object source) {
		final BeanWrapperImpl src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null)
				emptyNames.add(pd.getName());
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

}

class MyBean {
	private int id;
	private String name;
	private String addr;
	private char sex;
	private byte status;

	public MyBean() {
		super();
	}

	public MyBean(int id, String name, String addr, char sex, byte status) {
		super();
		this.id = id;
		this.name = name;
		this.addr = addr;
		this.sex = sex;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "MyBean [id=" + id + ", name=" + name + ", addr=" + addr + ", sex=" + sex + ", status=" + status + "]";
	}

}
