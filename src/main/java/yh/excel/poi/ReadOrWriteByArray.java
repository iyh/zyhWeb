package yh.excel.poi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import yh.web.util.ExecleUtil;

public class ReadOrWriteByArray {
	// DL_64人员名称坐标数组
	public static final String[][] addressDL64 = { { "B21", "B43", "B65", "B87" },
			{ "M18", "M24", "M40", "M46", "M62", "M68", "M84", "M90" },
			{ "W12", "W23", "W34", "W45", "W56", "W67", "W78", "W89" },
			{ "AG9", "AG15", "AG20", "AG26", "AG31", "AG37", "AG42", "AG48", "AG53", "AG59", "AG64", "AG70", "AG75",
					"AG81", "AG86", "AG92" },
			{ "AQ12", "AQ23", "AQ34", "AQ45", "AQ56", "AQ67", "AQ78", "AQ89" }, { "BA18", "BA40", "BA62", "BA84" } };

	public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException {
		// String[] c1 = { "B21", "B43", "B65", "B87" };
		// String[] c2 = { "M18", "M24", "M40", "M46", "M62", "M68", "M84",
		// "M90" };
		// String[] c3 = { "W12", "W23", "W34", "W45", "W56", "W67", "W78",
		// "W89" };
		// String[] c4 = { "AG9", "AG15", "AG20", "AG26", "AG31", "AG37",
		// "AG42", "AG48", "AG53", "AG59", "AG64", "AG70",
		// "AG75", "AG81", "AG86", "AG92" };
		// String[] c5 = { "AQ12", "AQ23", "AQ34", "AQ45", "AQ56", "AQ67",
		// "AQ78", "AQ89" };
		// String[] c6 = { "BA18", "BA40", "BA62", "BA84" };
		// addressDL64[0] = c1;
		// addressDL64[1] = c2;
		// addressDL64[2] = c3;
		// addressDL64[3] = c4;
		// addressDL64[4] = c5;
		// addressDL64[5] = c6;
		// for (int i = 0; i < addressDL64.length; i++) {
		// String[] sa = addressDL64[i];
		//
		// System.out.println(Arrays.toString(sa)+ " l="+sa.length);
		//
		// }
		String path = "E:/workspaces/eclipse_spaces/pool_ssgl/pool/WebRoot/temple/aginst/upload/DL_64 - 副本.xlsx";
		Workbook wb = ExecleUtil.getWorkbook(path);
		int wbSize = wb.getNumberOfSheets();
		System.out.println(wbSize);
		for (int i = 0; i < wbSize; i++) {
			System.out.println("页号："+(i+1));
			Sheet sheet = wb.getSheetAt(i);
			read(sheet);
		}

	}

	/**
	 * @Title:将双败64模板文件中的参赛人员姓名读取为数组
	 * @Description:Comment for non-overriding methods
	 * @author 张颖辉
	 * @date 2017年5月16日下午4:24:18
	 * @param sheet
	 * @return
	 */
	public static String[] read(Sheet sheet) {
		for (int i = 0; i < addressDL64.length; i++) {
			String[] sa = addressDL64[i];
			System.out.print("NO." + (i + 1) + " l=" + sa.length + " ");
			for (int j = 0; j < sa.length; j++) {
				String cellString = ExecleUtil.readCell2String(sheet, sa[j]);
				System.out.print("["+cellString + "] ");
			}
			System.out.print("\n");
		}
		return null;
	}

}
