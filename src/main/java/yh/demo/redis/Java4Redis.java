package yh.demo.redis;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Java4Redis {
	private static Logger logger = LoggerFactory.getLogger(Java4Redis.class);
	private static Jedis jedis;
	static {
		// 连接本地的 Redis 服务
		jedis = new Jedis("localhost");
		// 查看服务是否运行
		logger.info("Server is running: " + jedis.ping());
	}

	public static void main(String[] args) {
		logger.info("*************************jedisString()*****************************");
		jedisString();
		logger.info("**************************jedisList()****************************");
		 jedisList();
		logger.info("**************************jedisKeys()****************************");
		 jedisKeys();
	}

	public static boolean jedisString() {
		jedis.set("key", "value");
		jedis.set("name", "43124314234123");
		System.out.println(jedis.get("key"));
		System.out.println(jedis.get("name"));
		return true;
	}

	public static void jedisList() {
		jedis.lpush("list1", "Redis1");
		jedis.lpush("list1", "Redis2");
		jedis.lpush("list1", "Redis3");
		jedis.lpush("list1", "Redis4");
		jedis.lpush("list1", "Redis5");
		jedis.lpush("list1", "Redis6");
		jedis.lpush("list1", "Redis7");
		// 获取存储的数据并输出
		List<String> list = jedis.lrange("list1", 0, 100);
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Stored string in redis:: " + i + "::" + list.get(i));
		}
	}

	public static void jedisKeys() {
		// 获取数据并输出
		Set<String> keys = jedis.keys("*");
		for (String key : keys) {
			System.out.println("List of stored keys:: " + key);
		}

	}
	/**
	 * 使用连接池连接
	 * @Title:函数
	 * @Description:Comment for non-overriding methods
	 * @author 张颖辉
	 * @date 2017年2月17日上午10:57:31
	 */
	public void pool() {
		JedisPoolConfig config = new JedisPoolConfig();
		//最大连接数
		config.setMaxTotal(30);
		//最大连接空闲数
		config.setMaxIdle(2);
		
		JedisPool pool = new JedisPool(config, "192.168.101.3", 6379);
		Jedis jedis = null;

		try  {
			jedis = pool.getResource();
			
			jedis.set("name", "lisi");
			String name = jedis.get("name");
			System.out.println(name);
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(jedis != null){
				//关闭连接
				jedis.close();
			}
		}
		
	}
}
