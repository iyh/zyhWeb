package yh.net.rpc.tcp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.Socket;

/**
 * @Title:客户端
 * @Description:基于TCP实现远程方法调用(RPC)
 * @author 张颖辉
 * @date  2016年12月29日下午3:47:59
 * @version 1.0
 */
public class Client {
	public static void main(String[] args) throws Exception {
		new Consumer().callRemote();
	}

}

/**
 * @Title:服务的消费者（使用服务的客户端）
 * @Description:Comment for created type
 * @author 张颖辉
 * @date 2016年12月29日下午3:47:59
 * @version 1.0
 */
class Consumer {
	public void callRemote() throws Exception {
		Socket socket = new Socket("localhost", 7777);
		// 发送调用信息 接口名称 方法名称 参数类型 参数值
		Method method = SayHelloService.class.getMethod("sayHello", String.class);
		Object[] parameters = { "hello111" };
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		oos.writeUTF(SayHelloService.class.getName());
		oos.writeUTF(method.getName());
		oos.writeObject(method.getParameterTypes());
		oos.writeObject(parameters);
		oos.flush();
		// 接收返回数据
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		Object result = ois.readObject();
		System.out.println("【客】result=" + result);

		socket.close();
	}
}
