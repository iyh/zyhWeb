package yh.net.rpc.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Title:服务端
 * @Description:基于TCP实现远程方法调用(RPC)，性能高，实现复杂，开发代价高，基于HTTP应用层协议的更好
 * @author 张颖辉
 * @date  2016年12月29日下午3:47:59
 * @version 1.0
 */
public class Server {
	public static void main(String[] args) throws Exception {
		new Provider().accept();
	}
}

/**
 * @Title:服务接口
 * @Description:Comment for created type
 * @author 张颖辉
 * @date 2016年12月29日下午3:35:04
 * @version 1.0
 */
interface SayHelloService {
	public String sayHello(String helloArg);
}

/**
 * @Title:服务实现（运行在服务端可以被客户端远程访问）
 * @Description:Comment for created type
 * @author 张颖辉
 * @date 2016年12月29日下午3:35:30
 * @version 1.0
 */
class SayHelloServiceimpl implements SayHelloService {
	@Override
	public String sayHello(String helloArg) {
		if ("hello".equals(helloArg.trim())) {
			return "hello";
		}
		return "Bye!";
	}
}
/**
 * @Title:服务的提供者
 * @Description:Comment for created type
 * @author 张颖辉
 * @date 2017年2月4日下午1:44:48
 * @version 1.0
 */
class Provider {
	public void accept() throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ServerSocket serverSocket = new ServerSocket(7777);
		
		while (true) {
			System.out.println("【服务端】等待访问。。。。");
			Socket socket = serverSocket.accept();

			// 接收调用信息
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			String interfaceName = ois.readUTF();
			String methodName = ois.readUTF();
			Class<?>[] parameterTypes = (Class<?>[]) ois.readObject();
			Object[] parameters = (Object[]) ois.readObject();
			System.out.println("【服务端】parameters="+parameters);
			// 执行方法
			Class<?> interfaceClass = Class.forName(interfaceName);
			Object serviceImpl = getServiceImpl(interfaceName);
			System.out.println("【服务端】serviceImpl="+serviceImpl);
			Method method = interfaceClass.getMethod(methodName, parameterTypes);
			Object result = method.invoke(serviceImpl, parameters);
			System.out.println("【服务端】result="+result);
			// 返回结果
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(result);
			oos.flush();
		}
	}

	/**
	 * 
	 * @Title:通过接口获取实现类的实例对象(不知道是不是该这样实现)
	 * @Description:Comment for non-overriding methods
	 * @author 张颖辉
	 * @date 2016年12月29日下午4:23:34
	 * @param interfaceName
	 * @return
	 */
	public Object getServiceImpl(String interfaceName) {
		if (interfaceName.endsWith("SayHelloService")) {
			System.out.println("【服务端】interfaceName="+interfaceName);
			return new SayHelloServiceimpl();
		}
		return null;
	}

}
