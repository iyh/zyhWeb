package yh.net.http.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class MyHttpRequest {
	public static void main(String[] args) throws UnsupportedEncodingException {
		// for (int i = 11; i < 21; i++) {
		// sendPost(
		// "http://123.56.120.168:7070/HappyFootBall/school/addSuggest.do",
		// "user_id=用户"+i+"&content=post请求：中文乱码测试"+i);
		// sendGet(
		// "http://123.56.120.168:7070/HappyFootBall/school/addSuggest.do",
		// "user_id=user"+i+"&content=get请求：中文乱码测试"+i);
		// }

		/*** 参数encoding处理get中有空格的问题 ***/
		// System.out
		// .println(sendGet(
		// "http://localhost:8080/BilliardsMS/ylmatch.do",
		// "actionType=docreatematch&json="
		// + URLEncoder
		// .encode(
		// "{\"data\":[{\"matchMode\":\"6\",\"matchName\":\"斯诺克远程对战EncodeUF8 Snooker1220143113\",\"personnumber\":\"2\",\"startDate\":\"2016-12-20 14:47:03\",\"state\":\"1\"}]}",
		// "UTF-8")));
		// URLDecoder.decode(s, enc);
		for (int i = 0; true; i++) {
			System.out.println("i="+i);
			try {
				Thread.sleep(3600000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out
					.println(sendGet(
							// http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do?json={cell:"18832306420",appid:"97208126-47c9-44e5-956e-c3d4fd3e776c"}
							"http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do",
							"json={cell:\"13027629019\",appid:\"97208126-47c9-44e5-956e-c3d4fd3e776c\"}"));
			System.out
			.println(sendGet(
					// http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do?json={cell:"18832306420",appid:"97208126-47c9-44e5-956e-c3d4fd3e776c"}
					"http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do",
					"json={cell:\"13033977911\",appid:\"97208126-47c9-44e5-956e-c3d4fd3e776c\"}"));
			System.out
			.println(sendGet(
					// http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do?json={cell:"18832306420",appid:"97208126-47c9-44e5-956e-c3d4fd3e776c"}
					"http://happyfootball.lhzsports.com:7070/HappyFootBall/account/veriftyCell.do",
					"json={cell:\"13073758730\",appid:\"97208126-47c9-44e5-956e-c3d4fd3e776c\"}"));

		}

	}

	/**
	 * 向指定URL发送GET方法的请求
	 * 
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection
					.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

}
