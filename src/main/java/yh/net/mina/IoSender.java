package yh.net.mina;

import org.apache.mina.core.session.IoSession;

public class IoSender {
	public static void noticeMsg(IoSession session, Object msg) {
		synchronized (session) {
			session.write(msg);
		}
	}
}
