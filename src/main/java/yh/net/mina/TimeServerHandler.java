package yh.net.mina;

import java.util.Date;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import yh.net.mina.thread.T;

public class TimeServerHandler extends IoHandlerAdapter {
	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		cause.printStackTrace();
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String str = message.toString();
		System.out.println("ssss" + str);
		if (str.trim().equalsIgnoreCase("quit")) {
			session.close();
			return;
		}
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		System.out.println("IDLE " + session.getIdleCount(status));
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		new Thread(new T(session, "1")).start();
		new Thread(new T(session, "2")).start();
		new Thread(new T(session, "3")).start();
		new Thread(new T(session, "4")).start();
		new Thread(new T(session, "5")).start();
		new Thread(new T(session, "6")).start();
		new Thread(new T(session, "7")).start();
	}
}
